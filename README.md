# odor_cups

This repository holds 3D printable odor cups which were inspired by
odor cups described in [Michels et al., 2017](https://www.frontiersin.org/articles/10.3389/fnbeh.2017.00045/full)

Both parts have been printed using a Form2 3D printer (Formlabs).